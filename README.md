# DROWL Project Settings

The DROWL Project Settings module adds a page for configuring basic information about the project, e.g. contact, owners, legal information, etc. while providing custom tokens for global access to this information and automatic
import from the [Site Settings](https://www.drupal.org/project/site_settings) and [Config Pages](https://www.drupal.org/project/config_pages) Drupal modules.


## Requirements

This module requires the following modules:

- [Address](https://www.drupal.org/project/address)
- [Token](https://www.drupal.org/project/token)


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- If you were already using the [Site Settings](https://www.drupal.org/project/site_settings) or [Config Pages](https://www.drupal.org/project/config_pages) module on your site:
  - DROWL Project Settings will automatically import your project information. You will receive a notification regarding that.
  - Uninstall your old module so that there will be no conflicts between the two modules.
- If you weren't using the modules mentioned above or did not receive the notification regarding the automatic import:
  - Go to *Administration > Configuration > System > Project information* and enter the information about the project.
