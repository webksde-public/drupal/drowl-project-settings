<?php

declare(strict_types=1);

namespace Drupal\drowl_project_settings\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a DROWL Project Settings form.
 */
final class ConfigPagesImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'drowl_project_settings_config_pages_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // @todo Fix remaining todo's in this file.
    $this->messenger()->addError('This is not properly implemented yet!');
    // $form['actions'] = [
    //   '#type' => 'actions',
    //   'submit' => [
    //     '#type' => 'submit',
    //     '#value' => $this->t('Import config pages'),
    //   ],
    // ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!$this->configPagesInstalled()) {
      $this->messenger()->addError('The Config Pages (config_pages) module is not installed, values can not get imported.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->importFromConfigPages();
  }

  /**
   * Checks, whether the "config_pages" module is installed.
   *
   * @todo Also check if a config page exists here:
   */
  public function configPagesInstalled() : bool {
    if (\Drupal::service('module_handler')->moduleExists('config_pages')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Imports pre-defined settings from the Config Pages module.
   */
  private function importFromConfigPages() {
    if (!$this->configPagesInstalled()) {
      return;
    }
    $configPagesLoader = \Drupal::service('config_pages.loader');
    // @todo From @JS: This is hardcoded to "website_settings", I don't know
    // what the original feature request was, but I don't think this is correct?
    $address = $configPagesLoader->getValue('website_settings', 'field_address')[0];
    $geoCoordinates = $configPagesLoader->getValue('website_settings', 'field_geo_coordinates')[0]['value'];
    $mail = $configPagesLoader->getValue('website_settings', 'field_mail')[0]['value'];
    $tel = $configPagesLoader->getValue('website_settings', 'field_phone')[0]['value'];
    $fax = $configPagesLoader->getValue('website_settings', 'field_fax')[0]['value'];
    $vatId = $configPagesLoader->getValue('website_settings', 'field_vat_id')[0]['value'];
    $companyLaw = $configPagesLoader->getValue('website_settings', 'field_company_law')[0];
    $projectOwner = $configPagesLoader->getValue('website_settings', 'field_project_owner')[0]['value'];
    $descriptionShort = $configPagesLoader->getValue('website_settings', 'field_description_short')[0]['value'];
    $copyrights = $configPagesLoader->getValue('website_settings', 'field_copyrights')[0];
    $this->setProjectInformationValues(
      address: $address,
      geoCoordinates: $geoCoordinates,
      mail: $mail,
      tel: $tel,
      fax: $fax,
      vatId: $vatId,
      companyLaw: $companyLaw,
      projectOwner: $projectOwner,
      descriptionShort: $descriptionShort,
      copyrights: $copyrights
    );
    $this->messenger()->addMessage('Project Information values from the Config Pages (config_pages) module have been found and imported. You may now uninstall the Config Pages (config_pages) module. Please ensure to replace all existing tokens with the new ones.');
  }

  /**
   * Sets the values of the config to the imported config values.
   *
   * @param array $address
   *   The address value.
   * @param string $geoCoordinates
   *   The geoCoordinates value.
   * @param string $mail
   *   The mail value.
   * @param string $tel
   *   The tel value.
   * @param string $fax
   *   The fax value.
   * @param string $vatId
   *   The vatId value.
   * @param array $companyLaw
   *   The companyLaw value.
   * @param string $projectOwner
   *   The projectOwner value.
   * @param string $descriptionShort
   *   The descriptionShort value.
   * @param array $copyrights
   *   The copyrights value.
   */
  private function setProjectInformationValues(array $address, $geoCoordinates, $mail, $tel, $fax, $vatId, array $companyLaw, $projectOwner, $descriptionShort, array $copyrights) {
    $this->configFactory()->getEditable('drowl_project_settings.settings')
      ->set('address', $address)
      ->set('geo_coordinates', $geoCoordinates)
      ->set('mail', $mail)
      ->set('tel', $tel)
      ->set('fax', $fax)
      ->set('vat_id', $vatId)
      ->set('company_law', $companyLaw)
      ->set('project_owner', $projectOwner)
      ->set('description_short', $descriptionShort)
      ->set('copyrights', $copyrights)
      ->save();
  }

}
