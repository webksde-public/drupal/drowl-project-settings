<?php

/**
 * @file
 * Install, update and uninstall functions for the DPS module.
 */

declare(strict_types=1);

use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 */
function drowl_project_settings_requirements($phase) {
  $requirements = [];
  if ($phase !== 'runtime') {
    return [];
  }

  $anyRequiredValueEmpty = FALSE;
  $moduleConfig = \Drupal::service('config.factory')->get('drowl_project_settings.settings')->get();
  if (array_key_exists('address', $moduleConfig)) {
    $requiredValues = [
      $moduleConfig['address']['country_code'],
      $moduleConfig['address']['address_line1'],
      $moduleConfig['address']['postal_code'],
      $moduleConfig['address']['locality'],
      $moduleConfig['mail'],
      $moduleConfig['tel'],
      $moduleConfig['vat_id'],
      $moduleConfig['company_law']['value'],
      $moduleConfig['project_owner'],
      $moduleConfig['description_short'],
      $moduleConfig['copyrights']['value'],
    ];
    foreach ($requiredValues as $requiredValue) {
      if ($requiredValue == NULL || $requiredValue == '') {
        $anyRequiredValueEmpty = TRUE;
      }
    }
  }
  else {
    $anyRequiredValueEmpty = TRUE;
  }

  if ($anyRequiredValueEmpty) {
    $requirements['drowl_project_settings'] = [
      'title' => t('DROWL Project Settings'),
      'severity' => REQUIREMENT_WARNING,
      'value' => t('Project Information missing'),
      'description' => t('Not all relevant values have been filled yet.<br><a href="@link"><i>Configure them here.</i></a>', ['@link' => Url::fromRoute('drowl_project_settings.settings')->toString()]),
    ];
  }

  return $requirements;
}

/**
 * Internally rename "phone" config to "tel".
 */
function drowl_project_settings_update_9001() {
  $config = \Drupal::configFactory()->getEditable('drowl_project_settings.settings');
  if (!empty($phone = $config->get('phone'))) {
    $config->set('tel', $phone);
  }
  $config->clear('phone')
    ->save();
}

/**
 * Rename "email" config to "mail" (Drupal standard).
 */
function drowl_project_settings_update_9002() {
  $config = \Drupal::configFactory()->getEditable('drowl_project_settings.settings');
  $config->set('mail', $config->get('email') ?? '')
    ->clear('email')
    ->save();
}
