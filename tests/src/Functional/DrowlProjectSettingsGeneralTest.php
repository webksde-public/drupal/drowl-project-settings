<?php

namespace Drupal\Tests\drowl_project_settings\Functional;

/**
 * This class provides methods specifically for testing something.
 *
 * @group drowl_project_settings
 */
class DrowlProjectSettingsGeneralTest extends DrowlProjectSettingsTestBase {

  /**
   * Sets up the drowl_project_settings config.
   *
   * Note, that this requires "filter_test", hence we can not move it into
   * "DrowlProjectSettingsTestBase".
   */
  protected function setupDrowlProjectSettingsConfig() {
    $this->config('drowl_project_settings.settings')
      ->set('address', [
        'country_code' => 'DE',
        'given_name' => 'FirstName',
        'family_name' => 'LastName',
        'organization' => 'CompanyName',
        'address_line1' => 'AddressLine1',
        'address_line2' => 'AddressLine2',
        'postal_code' => '12345',
        'locality' => 'CityName',
      ])
      ->set('geo_coordinates', '123123.456456')
      ->set('mail', 'email@server.com')
      ->set('tel', '+49 123 456789')
      ->set('fax', '+49 123 456789')
      ->set('vat_id', 'VatID')
      ->set('company_law', [
        'value' => 'CompanyLaw',
        'format' => 'filter_test',
      ])
      ->set('project_owner', 'ProjectOwner')
      ->set('description_short', 'ShortDescription')
      ->set('copyrights', [
        'value' => 'Copyrights',
        'format' => 'filter_test',
      ])
      ->save();
  }

  /**
   * Sets up the token filter for the filter_test text format.
   *
   * Note, that this requires "filter_test", hence we can not move it into
   * "DrowlProjectSettingsTestBase".
   */
  protected function setUpTokenFilter() {
    $edit = [
      'edit-filters-token-filter-status' => 1,
      'edit-filters-filter-html-escape-status' => 0,
    ];
    $this->drupalGet('admin/config/content/formats/manage/filter_test');
    $this->submitForm($edit, 'Save configuration');
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'token_filter',
    'filter_test',
  ];

  /**
   * {@inheritdoc}
   *
   * Use different theme, so we can utilize the css selectors.
   */
  protected $defaultTheme = 'olivero';

  /**
   * Tests the functionality of the settings page.
   */
  public function testSettingsChange() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to the settings page and set the values.
    $this->drupalGet('/admin/config/system/project-information');
    $session->statusCodeEquals(200);
    $page->selectFieldOption('address[country_code]', 'DE');
    $page->fillField('address[given_name]', 'FirstName');
    $page->fillField('address[family_name]', 'LastName');
    $page->fillField('address[organization]', 'CompanyName');
    $page->fillField('address[address_line1]', 'AddressLine1');
    $page->fillField('address[address_line2]', 'AddressLine2');
    $page->fillField('address[postal_code]', '12345');
    $page->fillField('address[locality]', 'CityName');
    $page->fillField('geo_coordinates', '123123.456456');
    $page->fillField('mail', 'mail@server.com');
    $page->fillField('tel', '1234/567890');
    $page->fillField('fax', '+49 123 456789');
    $page->fillField('vat_id', 'VatID');
    $page->fillField('company_law[value]', 'CompanyLaw');
    $page->fillField('project_owner', 'ProjectOwner');
    $page->fillField('description_short', 'ShortDescription');
    $page->fillField('copyrights[value]', 'Copyrights');
    // Save the settings and check if they are still there.
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->fieldValueEquals('address[country_code]', 'DE');
    $session->fieldValueEquals('address[given_name]', 'FirstName');
    $session->fieldValueEquals('address[family_name]', 'LastName');
    $session->fieldValueEquals('address[organization]', 'CompanyName');
    $session->fieldValueEquals('address[address_line1]', 'AddressLine1');
    $session->fieldValueEquals('address[address_line2]', 'AddressLine2');
    $session->fieldValueEquals('address[postal_code]', '12345');
    $session->fieldValueEquals('address[locality]', 'CityName');
    $session->fieldValueEquals('geo_coordinates', '123123.456456');
    $session->fieldValueEquals('mail', 'mail@server.com');
    $session->fieldValueEquals('tel', '1234/567890');
    $session->fieldValueEquals('fax', '+49 123 456789');
    $session->fieldValueEquals('vat_id', 'VatID');
    $session->fieldValueEquals('company_law[value]', 'CompanyLaw');
    $session->fieldValueEquals('project_owner', 'ProjectOwner');
    $session->fieldValueEquals('description_short', 'ShortDescription');
    $session->fieldValueEquals('copyrights[value]', 'Copyrights');
  }

  /**
   * Tests the functionality of the address formatted default token.
   */
  public function testAddressFormattedDefaultToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:address_formatted:default]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName AddressLine1AddressLine2 DE-12345 CityName');
  }

  /**
   * Tests the functionality of the address formatted token.
   */
  public function testAddressFormattedToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:address_formatted]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName AddressLine1AddressLine2 DE-12345 CityName');
  }

  /**
   * Tests the functionality of the address formatted plain token.
   */
  public function testAddressFormattedPlainToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:address_formatted:plain]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName FirstName LastName AddressLine1 AddressLine2 DE-12345 CityName');
  }

  /**
   * Tests the functionality of the address formatted legal token.
   */
  public function testAddressFormattedLegalToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:address_formatted:legal]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    // Note, that we have an invisible character "U+00a0" in the string, this
    // can not be defined otherwise.
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName AddressLine1AddressLine2 DE-12345 CityName VAT-ID: VatID CompanyLaw');
  }

  /**
   * Tests the functionality of the address formatted short token.
   */
  public function testAddressFormattedShortToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:address_formatted:short]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName AddressLine1AddressLine2 DE-12345 CityName');
  }

  /**
   * Tests the functionality of the contact address formatted default token.
   */
  public function testContactAddressFormattedDefaultToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:contact_address_formatted:default]	',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName AddressLine1AddressLine2 DE-12345 CityName Tel: +49 123 456789 Fax: +49 123 456789 Mail: email@server.com');
  }

  /**
   * Tests the functionality of the contact address formatted token.
   */
  public function testContactAddressFormattedToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:contact_address_formatted]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName AddressLine1AddressLine2 DE-12345 CityName Tel: +49 123 456789 Fax: +49 123 456789 Mail: email@server.com');
  }

  /**
   * Tests the functionality of the contact address formatted icons token.
   *
   * @todo We do not test the actual icon, but this is good enough for now.
   */
  public function testContactAddressFormattedIconsToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:contact_address_formatted:icons]	',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'CompanyName AddressLine1AddressLine2 DE-12345 CityName +49 123 456789 +49 123 456789 email@server.com');
  }

  /**
   * Tests the functionality of the contact formatted default token.
   */
  public function testContactFormattedDefaultToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:contact_formatted:default]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'Tel: +49 123 456789 Fax: +49 123 456789 Mail: email@server.com');
  }

  /**
   * Tests the functionality of the contact formatted token.
   */
  public function testContactFormattedToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:contact_formatted]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'Tel: +49 123 456789 Fax: +49 123 456789 Mail: email@server.com');
  }

  /**
   * Tests the functionality of the contact formatted plain token.
   */
  public function testContactFormattedPlainToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:contact_formatted:plain]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'Tel: +49 123 456789 Fax: +49 123 456789 Mail: email@server.com');
  }

  /**
   * Tests the functionality of the contact formatted icons token.
   *
   * @todo We do not test the actual icon, but this is good enough for now.
   */
  public function testContactFormattedIconsToken() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:contact_formatted:icons]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', '+49 123 456789 +49 123 456789 email@server.com');
  }

  /**
   * Tests the functionality of all single value tokens.
   */
  public function testSingleValueTokens() {
    $session = $this->assertSession();
    $this->setupDrowlProjectSettingsConfig();
    $this->setUpTokenFilter();

    $node = $this->createNode([
      'body' => [
        'value' => '[project:mail] [project:fax] [project:geo_coordinates] [project:project_owner] [project:description_short] [project:tel] [project:vat_id] [project:company_law] [project:copyrights]',
        'format' => 'filter_test',
      ],
    ]);

    $this->drupalGet('/node/' . $node->id());
    $session->statusCodeEquals(200);
    $session->elementTextEquals('css', 'article > div.node__content > div.text-content', 'email@server.com +49 123 456789 123123.456456 ProjectOwner ShortDescription +49 123 456789 VatID CompanyLaw Copyrights');
  }

}
