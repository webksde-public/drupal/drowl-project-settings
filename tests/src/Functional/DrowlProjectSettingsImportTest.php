<?php

namespace Drupal\Tests\drowl_project_settings\Functional;

/**
 * This class provides methods specifically for testing something.
 *
 * @group drowl_project_settings
 */
class DrowlProjectSettingsImportTest extends DrowlProjectSettingsTestBase {

  /**
   * Tests the functionality of importing from the Config Pages module.
   */
  public function testImportConfigPages() {

    /*
     * This feature has been postponed for now.
     * @todo Implement this test if the feature is live.
     */

  }

}
