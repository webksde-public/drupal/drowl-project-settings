<?php

namespace Drupal\Tests\drowl_project_settings\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base class for drowl_project_settings functional testing.
 *
 * @group drowl_project_settings
 */
abstract class DrowlProjectSettingsTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_project_settings',
    'address',
    'token',
    'ckeditor5',
    'node',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->createContentType(['type' => 'page']);
    $this->drupalLogin($this->adminUser);
  }

}
